import React, { Component } from 'react';
import Search from '../../components/search/Search';
import logo from '../../assets/images/logo.png';
import CONSTANTS from '../../assets/data/constants.json';
import STRINGS from '../../assets/data/strings.json';
import './Header.css';

class Header extends Component {

  render() {
    return (
      <header className={CONSTANTS.CLASSES.HEADER.MAIN}>
        <div className={CONSTANTS.CLASSES.HEADER.CONTENT}>
          <div className={CONSTANTS.CLASSES.HEADER.LOGO}>
            <img src={logo} alt={STRINGS.LOGO_ALT} />
          </div>
          <Search />
        </div>
      </header>
    );
  }
}

export default Header;
