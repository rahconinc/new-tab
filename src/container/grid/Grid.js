import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CONSTANTS from '../../assets/data/constants.json';
import './Grid.css';

class Grid extends Component {

  createHeader() {
    let header, subHeadline;
    if(this.props.headline!==undefined) {
      if(this.props.subHeadline!==undefined) {
        subHeadline = <h2 className={CONSTANTS.CLASSES.GRID.HEADLINE + ' ' + CONSTANTS.CLASSES.GRID.DOUBLEDHEADLINE}>{this.props.headline}
        <br/> <span className={CONSTANTS.CLASSES.GRID.SUBHEADLINE}>{this.props.subHeadline}</span></h2>
      } else {
        subHeadline = <h2 className={CONSTANTS.CLASSES.GRID.HEADLINE}>{this.props.headline}</h2>
      }
      header = <div className={CONSTANTS.CLASSES.GRID.HEADER}>
        {subHeadline}</div>
    }
    return header
  }

  render() {
    const {className, columns} = this.props;
    const gridItems = this.props.children;
    return (
      <div className={className + ' ' + CONSTANTS.CLASSES.GRID.MAIN} data-columns={columns}>
        {this.createHeader()}
        <div className={CONSTANTS.CLASSES.GRID.WRAPPER}>
          {gridItems}
        </div>
      </div>
    );
  }
}

Grid.PropTypes = {
  className: PropTypes.string.isRequired,
  columns: PropTypes.number,
  headline: PropTypes.string,
  subHeadline: PropTypes.string
}

Grid.defaultProps = {
  columns: CONSTANTS.DEFAULTS.GRID_COLUMNS
}

export default Grid;
