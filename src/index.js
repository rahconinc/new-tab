import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Tab from './page/Tab';
import registerServiceWorker from './assets/registerServiceWorker';

ReactDOM.render(<Tab />, document.getElementById('root'));
registerServiceWorker();
