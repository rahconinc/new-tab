import React, { Component } from 'react';
import Header from '../container/header/Header';
import Button from '../components/button/Button';
import Grid from '../container/grid/Grid';
import GridItem from '../components/griditem/Griditem';
import SpaceItem from '../components/spaceitem/Spaceitem';
import Support from '../components/support/Support';
import Data from '../assets/data/content.json';
import CONSTANTS from '../assets/data/constants.json';
import STRINGS from '../assets/data/strings.json';
import './Tab.css';

class Tab extends Component {

  constructor(props) {
    super(props);
    this.state = {
      timeNow: new Date()
    };
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.setTime(),
      CONSTANTS.DEFAULTS.INTERVAL
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  setTime() {
    this.setState({
      timeNow: new Date()
    });
  }

  createShowsHeadlineWithClock() {
    return STRINGS.UPCOMINGSHOWS + this.state.timeNow.toLocaleTimeString([CONSTANTS.DEFAULTS.LOCALE], {hour: CONSTANTS.DEFAULTS.HOUR, minute:CONSTANTS.DEFAULTS.MINUTE})
  }

  buildGridItems(type, dataCategory) {
    return Data[dataCategory].map((date, index) =>
      <GridItem
        key={index}
        data={date}
        type={type}
      />
    );
  }

  render() {
    return (
      <div className={CONSTANTS.CLASSES.TAB.APP}>
        <Header/>
        <main className={CONSTANTS.CLASSES.TAB.MAIN}>
          <Grid className={CONSTANTS.CLASSES.TAB.SHOWS} headline={this.createShowsHeadlineWithClock()} columns={CONSTANTS.DEFAULTS.SHOWSCOLUMNS} >
            {this.buildGridItems(CONSTANTS.DEFAULTS.ITEM_TYPES.SHOWS, 'nextShows')}
          </Grid>
          <div className={CONSTANTS.CLASSES.TAB.LIVESTREAM}>
            <Button href={CONSTANTS.DEFAULTS.LIVESTREAM_LINK} textThin={STRINGS.BUTTON_LIVE_1} textBold={STRINGS.BUTTON_LIVE_2}/>
          </div>
          <SpaceItem/>
          <Grid className={CONSTANTS.CLASSES.TAB.POSTS + ' ' + CONSTANTS.CLASSES.TAB.BIG_EDGE} headline={STRINGS.ACTUALPOSTS_1} subHeadline={STRINGS.ACTUALPOSTS_2}>
            {this.buildGridItems(CONSTANTS.DEFAULTS.ITEM_TYPES.POSTS, 'posts')}
          </Grid>
          <Support/>
        </main>
      </div>
    );
  }

}

export default Tab;
