import React, { Component } from 'react';
import CONSTANTS from '../../assets/data/constants.json';
import STRINGS from '../../assets/data/strings.json';
import './Search.css';

class Search extends Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    window.open(CONSTANTS.DEFAULTS.GOOGLESEARCH + this.state.value, "_self");
  }

  render() {
    return (
      <div className={CONSTANTS.CLASSES.SEARCH.MAIN}>
        <form onSubmit={this.handleSubmit}>
          <input type="text" placeholder={STRINGS.GOOGLESEARCH} value={this.state.value} onChange={this.handleChange} />
          <input type="submit" value={STRINGS.INPUTTEXT} />
        </form>
      </div>
    );
  }
}

export default Search;
