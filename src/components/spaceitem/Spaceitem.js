import React, { Component } from 'react';
import PlanetPink from '../../assets/images/planetPink.png';
import CONSTANTS from '../../assets/data/constants.json';
import STRINGS from '../../assets/data/strings.json';
import './Spaceitem.css';

class SpaceItem extends Component {

  render() {
    return (
      <div className={CONSTANTS.CLASSES.SPACEITEM.MAIN + ' ' + CONSTANTS.CLASSES.SPACEITEM.LIGHT}>
        <img src={PlanetPink} alt={STRINGS.PLANET_ALT} />
      </div>
    );
  }
}

export default SpaceItem;
