import React, { Component } from 'react';
import Heart from '../../assets/images/heart.png';
import CONSTANTS from '../../assets/data/constants.json';
import STRINGS from '../../assets/data/strings.json';
import './Support.css';

class Support extends Component {

  render() {
    return (
      <div className={CONSTANTS.CLASSES.TAB.SUPPORT}>
        <a href={CONSTANTS.DEFAULTS.LINK} target={CONSTANTS.DEFAULTS.TARGET}>
          <img src={Heart} alt=''/>
          <span>{STRINGS.SUPPORT_1} <br/> <span>{STRINGS.SUPPORT_2}</span></span>
        </a>
      </div>
    );
  }
}

export default Support;
