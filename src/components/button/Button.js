import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CONSTANTS from '../../assets/data/constants.json';
import './Button.css';

class Button extends Component {

  render() {
    const {href, textThin, textBold} = this.props;
    return (
      <a className={CONSTANTS.CLASSES.BUTTON.MAIN} href={href} target={CONSTANTS.DEFAULTS.TARGET}>
        {textThin} <span>{textBold}</span>
      </a>
    );
  }

}

Button.PropTypes = {
  href: PropTypes.string,
  textThin: PropTypes.string,
  textBold: PropTypes.string
}

Button.defaultProps = {
  href: CONSTANTS.DEFAULTS.LINK
}

export default Button;
