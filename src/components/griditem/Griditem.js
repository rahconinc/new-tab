import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DummyPicture from '../../assets/images/dummy.jpg';
import CONSTANTS from '../../assets/data/constants.json';
import './Griditem.css';

class GridItem extends Component {

  constructor(props) {
    super(props);

    this.itemData = this.props.data;
    this.clickHandler = this.clickHandler.bind(this);
  }

  getOverlay() {
    if(this.props.type === CONSTANTS.DEFAULTS.ITEM_TYPES.POSTS && this.itemData.section === "Mediathek") {
      return <div className={CONSTANTS.CLASSES.GRIDITEM.OVERLAY}>{this.itemData.date} / {this.itemData.duration}</div>
    } else {
      return false;
    }
  }

  getPicture() {
    const pictureUrl = (this.itemData.pictureUrl==="")?DummyPicture:process.env.PUBLIC_URL + '/images/' + this.itemData.pictureUrl;
    return <figure className={CONSTANTS.CLASSES.GRIDITEM.PICTURE}>
              <img src={pictureUrl} alt={this.itemData.title} />
            </figure>;
  }

  getRibbon() {
    let ribbon;
    if(this.props.type === CONSTANTS.DEFAULTS.ITEM_TYPES.SHOWS) {
      ribbon = <div className={CONSTANTS.CLASSES.GRIDITEM.STARTTIME}>{this.itemData.startTime}</div>;
    } else {
      ribbon = <div className={CONSTANTS.CLASSES.GRIDITEM.SECTION}>{this.itemData.section}</div>
    }
    return ribbon;
  }

  getContent() {
    let content;
    if(this.props.type === CONSTANTS.DEFAULTS.ITEM_TYPES.SHOWS) {
      content = <div className={CONSTANTS.CLASSES.GRIDITEM.CONTENT}>
        <h4 className={CONSTANTS.CLASSES.GRIDITEM.HEADLINE}>{this.itemData.title}</h4>
        <p className={CONSTANTS.CLASSES.GRIDITEM.DESCRIPTION}>{this.itemData.description}</p>
        <small className={CONSTANTS.CLASSES.GRIDITEM.DURATION}>{this.itemData.duration}</small>
      </div>
    } else {
      content = <div className={CONSTANTS.CLASSES.GRIDITEM.CONTENT}>
        <h4 className={CONSTANTS.CLASSES.GRIDITEM.HEADLINE}>{this.itemData.title}</h4>
        <p className={CONSTANTS.CLASSES.GRIDITEM.DESCRIPTION}>{this.itemData.description}</p>
        <small className={CONSTANTS.CLASSES.GRIDITEM.CATEGORY}>{this.itemData.category}</small>
      </div>
    }
    return content;
  }

  getPrice() {
    return(this.itemData.price!==""&&this.itemData.price!==undefined)?<div className={CONSTANTS.CLASSES.GRIDITEM.PRICE}>{this.itemData.price}<span className={CONSTANTS.CLASSES.GRIDITEM.CURRENCY}>{CONSTANTS.DEFAULTS.CURRENCY}</span></div>:'';
  }

  itemHighlighted() {
    return (this.itemData.highlighted)?CONSTANTS.CLASSES.GRIDITEM.HIGHLIGHTED:'';
  }

  getLink() {
    return(this.itemData.link!==""&&this.itemData.link!==undefined)?this.itemData.link:'';
  }

  clickHandler(event) {
    event.preventDefault();
    if(this.itemData.link!==""&&this.itemData.link!==undefined){
      window.open(this.itemData.link, CONSTANTS.DEFAULTS.TARGET);
    }
  }

  render() {
    return (
      <div onClick={this.clickHandler} className={CONSTANTS.CLASSES.GRIDITEM.MAIN + ' ' + this.itemHighlighted()}>
          <div className={CONSTANTS.CLASSES.GRIDITEM.RIBBON}>
            {this.getRibbon()}
          </div>
          {this.getPicture()}
          {this.getOverlay()}
          {this.getContent()}
          {this.getPrice()}
      </div>
    );
  }
}

GridItem.PropTypes = {
  type: PropTypes.number,
  date: PropTypes.object.isRequired
}

GridItem.defaultProps = {
  type: CONSTANTS.DEFAULTS.ITEM_TYPES.SHOWS
}

export default GridItem;
